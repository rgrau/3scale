;;;; 3scale.lisp
;; http://www.3scale.net/support/service-management-api/

(in-package #:3scale)

;;; "3scale" goes here. Hacks and glory await!
(defparameter *provider-key* "6b3587fb07efe4463dd6f9c5cc608ccb")
(defparameter *app-key* "71519c750cd5845360ddb68db786d8cf")
(defparameter *app-id* "97addefa")
(defparameter *server-url* "http://su1.3scale.net/")

;;; utilities
(defun get-http-result (url)
  (format t "url:~a" url)
  (with-open-stream
      (input (drakma:http-request url :want-stream t ))
    (read-line input)))

(defun post-http-result (url content)
  (format t "url:~a" url)
  (format t "~%string: ~a" content)
  (drakma:http-request url :method :post :content content))

(defun parse-response (response)
  (cxml:parse response
              (cxml-xmls:make-xmls-builder)))

(defun children (sxml)
  (cddr sxml))

(defun value (node)
  (car (last node)))

(defun true (x) t)

(defun find-child (&rest args)
  (car (apply #'find-children args)))

(defun find-children (tag sxml &key (pred #'true) )
  (let ((children (children sxml)))
    (remove-if-not pred (remove-if-not (lambda (child) (equal (car child) tag)) children))))

(defun find-attribute (tag sxml &ke (pred #'true) )
  (let ((attributes (second sxml)))
    (car (remove-if-not pred (remove-if-not (lambda (child) (equal (car child) tag)) attributes)))))

(defun get-plans (response)
  (let* ((xml-structure (parse-response response))
         (plans (stp:filter-recursively (stp:of-name "plan") xml-structure)))
    plans))

(defun plans ()
  (let ((url "http://raitest.3scale.net/plans.xml"))
    (get-http-result url)))

(defun authorized-p (sxml)
  (find-child "authorized"
              sxml
              :pred (lambda (x) (equal (third x) "true"))))

(defun error-p (x) nil)

(defun sanitize-symbol (symbol)
  (string-downcase (substitute #\_ #\- (symbol-name symbol))))

(defun join-string-list (string-list &optional (separator "&"))
  "Concatenates a list of strings
   and puts separator (default to &) between the elements."
  (let ((formatted-string (concatenate 'string "~{~A~^" separator "~}")))
      (format nil formatted-string string-list)))

(defun compact (alist)
  (remove-if (lambda (pair) (null (second pair)))
               alist))

(defun flatten (structure)
  (cond ((null structure) nil)
        ((atom structure) `(,structure))
        (t (mapcan #'flatten structure))))

(defun build-query-string (alist &optional (prefix ""))
  "build string like param=value&param2=val2 unless value is null"
  (format nil "~{~a~a=~a~^&~}" (flatten (mapcar (curry #'cons prefix)
                                                (compact alist)))))
(defun curry (function &rest args)
    (lambda (&rest more-args)
      (apply function (append args more-args))))

(defun build-usage (usage-list &optional (prefix ""))
  "builds the usage part, returning prefix[a]=b&prefix[c]=d ..."
  (let ((params-list (flatten (mapcar (curry #'cons prefix)
                                      usage-list))))
    (format nil "~{~a[~a]=~a~^&~}" params-list)))

(defun do-report (sxml)
  (format t "~a" sxml))

;;; transaction utilities
(defstruct transaction app-id usage timestamp)
(setq foo (make-transaction :app-id 1234 :usage '((hits . 2))))

(defun encode-transactions (transactions)
  (let ((counter -1))
    (join-string-list (mapcar (lambda (transaction)
                                (encode-transaction transaction (incf counter) ))
                              transactions))))

(defun encode-transaction (transaction &optional (number 0))
  (concatenate 'string
               (build-query-string
                (list (list "app_id" (transaction-app-id transaction))
                      (list "timestamp" (transaction-timestamp transaction)))
                (format nil "transactions[~a]" number))
               "&" (build-usage (transaction-usage transaction)
                                (format nil "transactions[~a][usage]" number))))

;;; public functions
;;; TODO possible optimization with macro, to eliminate duplication
;;; TODO Usage is a list of lists
(defun authorize (provider-key app-id &key app-key referrer (no-body nil) usage)
  (let* ((url "transactions/authorize.xml")
         (sxml (parse-response
                (get-http-result
                 (format nil "~a~a?~a&~a"  *server-url* url
                              (build-query-string (list (list "provider_key" provider-key)
                                                        (list "app_id" app-id)
                                                        (list "app_key" app-key)
                                                        (list "referrer" referrer)
                                                        (list "no_body" no-body)))
                              (build-usage usage "usage"))))))
    sxml))

(defun 3scale-report (provider-key transactions)
  (let* ((url "transactions.xml"))
    (multiple-value-bind
          (body status headers uri stream must-close reason)
        (post-http-result
         (concatenate 'string  *server-url* url)
         (concatenate 'string (build-query-string (list (list "provider_key" provider-key))) "&"
                      (url-rewrite:url-encode (encode-transactions transactions))))
      (equalp status 202))))

;;; not working for the moment
;; (defun authrep (provider-key app-id &key app-key referrer no-body usage)
;;   (let* ((url "transactions/authrep.xml?")
;;          (sxml (parse-response
;;                (get-http-result
;;                 (concatenate 'string  *server-url* url
;;                               (build-query-string (list (list "provider_key" provider-key)
;;                                                         (list "app_id" app-id)
;;                                                         (list "app_key" app-key)
;;                                                         (list "referrer" referrer)
;;                                                         (list "no_body" no-body)))
;;                               "&" (build-usage usage))))))))


(defmacro js-send-request (url)
  "httpGetRequest(~a)")



;; (3scale-struct transaction usage timestamp balblabla)

;; (3scale-auth (prov-key app-id)
;;              "fdsafdsa"
;;              (set url "foo")
;;              (set response (get-method url prov-key app-id))
;;              (create-foo-structure-from (parse-response response "//xpath"))
;;              (create-foo-structure-from (parse-response response "//xpath"))
;;              (create-foo-structure-from (parse-response response
;;              "//xpath")))
(defmacro fn (expr) `#',(rbuild expr))
(defun rbuild (expr)
(if (or (atom expr) (eq (car expr) 'lambda))
expr
(if (eq (car expr) 'compose)
(build-compose (cdr expr))
(build-call (car expr) (cdr expr)))))
(defun build-call (op fns)
(let ((g (gensym)))
`(lambda (,g)
(,op ,@(mapcar #'(lambda (f)
`(,(rbuild f) ,g))
fns)))))
(defun build-compose (fns)
(let ((g (gensym)))
`(lambda (,g)
,(labels ((rec (fns)
(if fns
`(,(rbuild (car fns))
,(rec (cdr fns)))
g)))
(rec fns)))))

(fn (compose list 1+ 1+))
