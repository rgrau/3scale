(in-package #:3scale)

(defun generate-function (name url &rest args)
  (format t "function ~a(~{~a~^, ~}){~%" name args)
  (format t "  requestUrl(\"~a\", ~{~a~^, ~});~%" url args)
  (format t "}~%"))
