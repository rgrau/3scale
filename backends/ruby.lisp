
(in-package #:3scale)

(defvar *indent* 0)
(defvar *tabwidth* 2)
(defvar *out* t)

(defparameter *provider-key* "6b3587fb07efe4463dd6f9c5cc608ccb")
(defparameter *app-key* "71519c750cd5845360ddb68db786d8cf")
(defparameter *app-id* "97addefa")
(defparameter *server-url* "http://su1.3scale.net/")

(defun 3scale-concatenate (&rest strings)
  (emit "%w{~{~a~^ ~}}.join(\"\")~%" strings))

(defmacro 3scale-join (join-str &rest strings)
  `(emit "[~{\"~a\"~^, ~}].join(~a)~%" ,strings ,join-str))

(defmacro with-html-output ((var &optional stream
                                 &key prologue
                                      ((:indent *indent*) *indent*))
                            &body body)
  "Transform the enclosed BODY consisting of HTML as s-expressions
into Lisp code to write the corresponding HTML as strings to VAR -
which should either hold a stream or which'll be bound to STREAM if
supplied."
  (when (and *indent*
             (not (integerp *indent*)))
    (setq *indent* 0))
  (when (eq prologue t)
    (setq prologue *prologue*))
  `(let ((,var ,(or stream var)))
    ,(tree-to-commands body var prologue)))

(defun tree-to-commands (tree stream)
  (apply-to-tree #'transform tree)
  ;; (format stream "~{~a~%~}"
  ;;         (flatten (apply-to-tree #'transorm tree)))
  )

(defun apply-to-tree (fun tree)
  (cond ((null tree) nil)
        ((consp tree) (cons
                       (funcall fun (car tree))
                       (apply-to-tree fun (cdr tree))))
        (t (funcall fun tree))))

(defun transform (body)
  (cond
    ((atom body) body)
    ((equal 'def-method (car body))
     (def-method (second body) (third body) (fourth body)))
    ((equal '3scale-if (car body)))
    ((equal 'def-api (car body))
     (def-api name params body))
    ((equal 'join (car body))
     (3scale-join (second body) (cddr body)))))

;;; Structure helpers

(defun 3scale-form (x)
  (if (atom x)
      `(emit "~a~%" ,x)
      x))

(defmacro 3scale-form* (&rest lst)
  `(progn
    ,@(mapcar #'3scale-form lst)))

(defmacro with-increased-indent (&body body)
  `(progn
     (incf *indent* *tabwidth*)
     ,@body
     (decf *indent* *tabwidth*)))

;;; Ruby statements
(defmacro 3scale-defclass (name &body body)
  `(progn
    (emit "class ~a ~%" ,(string-capitalize (symbol-name name)))
    (with-increased-indent
      ,@body)
    (emit "end~%")))

(defmacro 3scale-defun (name args &body body)
  `(progn
     (emit "def ~(~a~)(~(~{~(~a~)~^, ~}~))~%" ',name ',args)
     (with-increased-indent
       (3scale-form* ,@body))
     (emit "end~%")))

(defmacro 3scale-if (condition true-block false-block)
  `(progn
     (emit "if(~a)~%" ,condition)
     (with-increased-indent
       ,(3scale-form true-block))
     (emit "else~%")
     (with-increased-indent
       ,(3scale-form false-block))
     (emit "end~%")))

(defmacro 3scale-= (to &optional (value ""))
  `(emit "~(~a = ~a ~)~%" ,to ,value))

;; ;; (define-api "get-accounts" "accounts.xml" (provider-key user-id))
;; ;;; (define-api "get-plans" "plans.xml" '(provider-key user-id))
;; (defmacro define-api (name url parameters return-type xpath)
;;   `(progn
;;      (3scale-defun ,name ,parameters
;;        (let ((get-url ,(concat *server-url* "/" url "?" (join-string-list parameters))))
;;          (3scale-= "result" )
;;          (get-request get-url)
;;         ;; (mapcar
;;         ;;  (lambda (x) (create-object return-type x))
;;         (parse-xml-for-many "result" ,xpath))
;;         ))))

(defun ensure-list (arg)
  (if (not (listp arg)) (list arg) arg))

(defmacro define-api (name method url parameters return-type xpath)
  `(progn
     (3scale-defun ,name ,(remove-if (complement #'symbolp) (append (ensure-list url) parameters))
       (let ((get-url ,(concat *server-url* "/"
                               (format nil "~(~{~a/~}~)"
                                       (mapcar #'(lambda (x)
                                                   (if (symbolp x)
                                                       (concatenate 'string "#{" (symbol-name x) "}")
                                                       x))
                                               (ensure-list url)))))
               (parameters-for-url ,(join-string-list parameters)))
         (3scale-= "result" )
         ,(if (equalp method "get")
             `(get-request (concatenate 'string get-url "?" parameters-for-url))
             (if (equalp method "post")
                 '(post-request get-url parameters-for-url)
                 '(put-request get-url parameters-for-url)))
         ;; (mapcar
         ;;  (lambda (x) (create-object return-type x))
         )
       (parse-xml-for-many "result" ,xpath))))

(defun join-string-list (string-list)
  "prepares the query string according to string-list and puts
   separatorbetween the elements."
  (format nil "~{~a~^&~}"
          (mapcar (lambda (x)
                    (format nil "~(~a~)=#{~(~a~)}" x x))
                  string-list)))

(defmacro 3scale-attrs (&rest attrs)
  (emit "attr_accessor ~{:~a~^, ~}~%" (mapcar (compose #'string-downcase #'symbol-name) attrs)))

;;; Lisp utilities
(defun concat (&rest args)
  (format nil "~{~a~}" args))

(defun compose (&rest fns)
  #'(lambda (arg)
      (reduce #'(lambda (v f) (funcall f v))
              (reverse fns)
              :initial-value arg)))

(defun curry (function &rest args)
    (lambda (&rest more-args)
      (apply function (append args more-args))))

(defun flatten (structure)
  (cond ((null structure) nil)
        ((atom structure) `(,structure))
        (t (mapcan #'flatten structure))))

(defun emit (fmt &rest args)
  (write-string (make-string *indent* :initial-element #\Space) *out*)
  (apply #'format *out* args))

;;; Ruby utilities

(defmacro put-request (uri content)
  `(emit "Net::HTTP::Put(\"~a\", \"~a\")~%" ,uri ,content))

(defmacro get-request (uri)
  `(emit "Net::HTTP.get_form(\"~a\")~%" ,uri))

(defmacro post-request (uri content)
  `(emit "Net::HTTP.post_form(\"~a\", \"~a\")~%" ,uri ,content))

(3scale-defun get-and-parse (uri)
  `(get-request ,uri))

(defmacro parse-xml-for-many (xml search-for)
  `(emit "~a.xpath(\"~a\")~%" ,xml ,search-for))

(defmacro parse-xml-for-single (xml search-for)
  `(emit "~a.xpath(\"~a\"~%).first" ,xml ,search-for))
