(in-package #:3scale)

;;; (DEFINE-API-WITH-CUSTOM-URL function ("/accounts/" id "/plans.xml") (provider_key) "acc" "//accounts")
;;; (define-api get-accounts-by-state "/admin/api/accounts.xml" (provider_key state) "Account" "//account")

(defmacro define-api (name url parameters)
  )

(3scale-defclass Account
  (3scale-attrs org_name state id plans users))

(3scale-defclass User
  (3scale-attrs id username role))

(3scale-defclass ThreescaleConnections
  (define-api "get_accounts" '(provider_key) "Accounts" "//accounts"))



(3SCALE-DEFCLASS Accountmanagement
  (define-api get-accounts "get" "/admin/api/accounts.xml" (provider_key) "Account" "//accounts")
  (define-api get-accounts-by-state "get" "/admin/api/accounts.xml" (provider_key state) "Account" "//account")
  (3scale-defun get-pending-accounts (provider_key) "get-accounts-by-state(provider_key,\"pending\")")
  (3scale-defun get-rejected-accounts (provider_key) "get-accounts-by-state(provider_key,\"rejected\")")
  (3scale-defun get-approved-accounts (provider_key) "get-accounts-by-state(provider_key,\"approved\")")

  (DEFINE-API get-account-plan "get" ("admin/api/accounts" id "plan.xml") (id provider_key) "Account" "//account")

  ;; put
  (DEFINE-API change-plan "get" ("/admin/api/accounts" id "change_plan.xml") (provider_key plan_id) "Plan" "//plan")
  (DEFINE-API service-plans-for-account "get"
    ("/admin/api/accounts" id "service_plans.xml")
    (provider_key) "Plans" "//plans")

  ;; write POST
  (define-api buy-service-plan-for-account "post"
    ( "/admin/api/accounts" account_id "service_plans" id "buy.xml")
    (provider_key) "Plan" "//plan")

  (DEFINE-API app-plans-for-accounts "get" ("/admin/api/accounts" id "application_plans.xml") (provider_key) "Plan" "//plans")

  ;; post
  (DEFINE-API buy-application-plan-for-account "post" ("/admin/api/accounts" account_id "application_plans" id "buy.xml") (provider_key) "Plan" "//plan")

    )
;  (define-api get-accounts "admin/api/applications.xml" (provider_key) "Account" "//accounts")

 ; (define-api get-accounts "admin/api/applications.xml"
 ; (provider_key) "Account" "//accounts")
  )
