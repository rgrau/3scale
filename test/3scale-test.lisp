(defpackage :3scale-tests
  (:use :common-lisp :lisp-unit :3scale))

(in-package :3scale-tests)

(defun build-query-string (&rest args)
  (apply #'3scale::build-query-string args))

(define-test build-query-string
  (assert-equal "" (build-query-string '(())))
  (assert-equal "app_id=foo&prov_key=23"
                (3scale::build-query-string '(("app_id" "foo") ("prov_key" "23")))))

(define-test build-usage
  (assert-equal "[foo]=32" (3scale::build-usage '(("foo" 32))))
  (assert-equal "[foo]=bar" (3scale::build-usage '(("foo" "bar"))))
  (assert-equal "transaction[0][foo]=32&transaction[0][baz]=quux"
                (3scale::build-usage '(("foo" 32) ("baz" "quux"))
                                     "transaction[0]"))
  (assert-equal "[foo]=32&[baz]=quux" (3scale::build-usage '(("foo" "32") ("baz" "quux"))))

  )

(define-test join-string-list
  (assert-equal "hola 23" (3scale::join-string-list '("hola" 23) " "))
  (assert-equal "hola&23" (3scale::join-string-list '("hola" 23))))

(define-test encode-transactions
  (assert-equal
   "app_id=foo&transactions[0][usage][hits]=2&app_id=lalal&transactions[1][usage][hits]=32&transactions[1][usage][foo]=99"
   (3scale::encode-transactions (list (3scale::make-transaction :app-id "foo" :usage '(("hits" 2)))
                              (3scale::make-transaction :app-id "lalal" :usage '(("hits" 32) ("foo" 99)))))))
