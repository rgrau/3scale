(in-package #:3scale)

(defstruct usage-report period metric start end current-value)

(defun report-stub ()
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><status><authorized>true</authorized><plan>Custom testplan</plan><usage_reports><usage_report metric=\"hits\" period=\"minute\"><period_start>2011-11-04 00:01:00 +0000</period_start><period_end>2011-11-04 00:02:00 +0000</period_end><max_value>100</max_value><current_value>0</current_value></usage_report></usage_reports></status>")

(defun get-plan (sxml)
  (find-child "plan" sxml))

(defun get-reports (sxml)
  (mapcar
   (lambda (report)
     (make-usage-report :period (value (find-attribute "period" report))
                        :metric (value (find-attribute "metric" report))
                        :start (value (find-child "period_start" report))
                        :end (value (find-child "period_end" report))
                        :current-value (value (find-child "current_value" report))))
   (children (find-child "usage_reports" sxml))))
