;;;; 3scale.asd

(asdf:defsystem #:3scale
  :serial t
  :depends-on (#:cl-ppcre
               #:drakma
               #:cxml
               #:lisp-unit
               #:url-rewrite
               #:cxml-stp
               #:xpath)
  :components ((:file "package")
               (:file "report")
               (:file "backends/ruby")
               (:file "backends/javascript")
               (:file "3scale")))
